﻿//custom slick slider
'use strict';

$('.slide').slick({
    centerMode: true,
    centerPadding: '185px',
    slidesToShow: 1,
    arrows: false,
    dots: true,
    responsive: [{
        breakpoint: 768,
        settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 3
        }
    }, {
        breakpoint: 480,
        centerMode: false,
        settings: "unslick"
    }]
});
$('.slider').slick({
    arrows: false,
    dots: true
});

//Scroll to Top
$(window).scroll(function () {
    if ($(this).scrollTop() >= 0) {
        $('#return-to-top').fadeIn(200);
    } else {
        $('#return-to-top').fadeOut(200);
    }
});
$('#return-to-top').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 500);
});

